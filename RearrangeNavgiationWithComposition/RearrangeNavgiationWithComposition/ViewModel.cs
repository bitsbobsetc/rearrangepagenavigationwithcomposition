﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RearrangeNavigationWithComposition
{
    public class ViewModel
    {
        public ViewModel()
        {
            Items = new ObservableCollection<string>();

            for (int i = 0; i < 10; i++)
            {
                Items.Add($"Image {i}");
            }
        }

        public ObservableCollection<string> Items { get; private set; }
    }
}
