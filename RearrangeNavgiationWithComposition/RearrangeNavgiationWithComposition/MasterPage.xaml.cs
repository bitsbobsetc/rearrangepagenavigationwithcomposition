﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using RearrangeNavigationWithComposition.CompositionAPI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RearrangeNavigationWithComposition
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MasterPage : Page
    {
        private ConnectedTransition currentTransition;
        private CompositionImage clickedImage;

        public MasterPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;

            DataContext = new ViewModel();

            Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if ((this.currentTransition != null) && (this.clickedImage != null))
            {
                ScrollViewer scrollViewer = VisualTreeHelperExtensions.GetFirstDescendantOfType<ScrollViewer>(this.MainListView);

                // Kick off the transition now that the page has loaded
                this.currentTransition.Start(this.MainGrid, this.clickedImage, scrollViewer, this.MainListView);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
            base.OnNavigatedTo(e);

            // Store the incoming parameter
            if (e.Parameter is ConnectedTransition)
            {
                this.currentTransition = (ConnectedTransition)e.Parameter;
            }
            else
            {
                // Should not run ConnectedTransition
                this.currentTransition = null;
            }

            //Hide the back button on the list page as there is no where to go back to. 
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = Windows.UI.Core.AppViewBackButtonVisibility.Collapsed;
        }
    
        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button currentButton = sender as Button;

            var itemImage = currentButton?.Content as CompositionImage;
            if (itemImage == null)
            {
                return;
            }

            this.clickedImage = itemImage;

            // Setup the new transition and trigger the navigation
            ConnectedTransition transition = new ConnectedTransition();
            transition.Initialize(Frame, itemImage, null);

            Frame.Navigate(typeof(DetailsPage), transition);
        }
    }
}
