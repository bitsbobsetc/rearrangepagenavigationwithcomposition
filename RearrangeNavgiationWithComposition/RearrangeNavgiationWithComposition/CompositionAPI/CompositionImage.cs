﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Numerics;
using Windows.UI;
using Windows.UI.Composition;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Hosting;
using Windows.UI.Xaml.Media;

namespace RearrangeNavigationWithComposition.CompositionAPI
{
    public sealed class CompositionImage : Control
    {
        private bool _unloaded;
        private Compositor _compositor;
        private SpriteVisual _sprite;
        private Uri _uri;
        private CompositionDrawingSurface _surface;
        private CompositionSurfaceBrush _surfaceBrush;
        private CompositionBrush _brush;
        private LoadTimeEffectHandler _loadEffectDelegate;
        private CompositionStretch _stretchMode;
        private DispatcherTimer _timer;
        public event RoutedEventHandler ImageOpened;
        public event RoutedEventHandler ImageFailed;
        private TimeSpan _placeholderDelay;
        private CompositionBrush _placeholderBrush;
        private bool _sharedSurface;

        static private CompositionBrush _defaultPlaceholderBrush;
        static private ScalarKeyFrameAnimation
                                            _fadeOutAnimation;
        static private Vector2KeyFrameAnimation
                                            _scaleAnimation;
        static bool _staticsInitialized;

        public CompositionImage()
        {
            this.DefaultStyleKey = typeof(CompositionImage);
            this.Background = new SolidColorBrush(Colors.Transparent);
            this._stretchMode = CompositionStretch.Uniform;
            this.Loading += CompImage_Loading;
            this.Unloaded += CompImage_Unloaded;
            this.SizeChanged += CompImage_SizeChanged;

            this._compositor = ElementCompositionPreview.GetElementVisual(this).Compositor;

            // Intialize the statics as needed
            if (!_staticsInitialized)
            {
                _defaultPlaceholderBrush = this._compositor.CreateColorBrush(Colors.DarkGray);

                TimeSpan duration = TimeSpan.FromMilliseconds(1000);
                _fadeOutAnimation = this._compositor.CreateScalarKeyFrameAnimation();
                _fadeOutAnimation.InsertKeyFrame(0, 1);
                _fadeOutAnimation.InsertKeyFrame(1, 0);
                _fadeOutAnimation.Duration = duration;

                _scaleAnimation = this._compositor.CreateVector2KeyFrameAnimation();
                _scaleAnimation.InsertKeyFrame(0, new Vector2(1.25f, 1.25f));
                _scaleAnimation.InsertKeyFrame(1, new Vector2(1, 1));
                _scaleAnimation.Duration = duration;

                _staticsInitialized = true;
            }

            // Initialize the surface loader if needed
            if (!SurfaceLoader.IsInitialized)
            {
                SurfaceLoader.Initialize(ElementCompositionPreview.GetElementVisual(this).Compositor);
            }

            this._placeholderDelay = TimeSpan.FromMilliseconds(50);
            this._surfaceBrush = this._compositor.CreateSurfaceBrush(null);
        }

        private void ReleaseSurface()
        {
            if (this._surface != null)
            {
                // If no one has asked to share, dispose it to free the memory
                if (!this._sharedSurface)
                {
                    this._surface.Dispose();
                    this._surfaceBrush.Surface = null;
                }
                this._surface = null;
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            Size desiredSize = new Size(0, 0);

            // We override measure to implement similar semantics to the normal XAML Image UIElement
            if (this._surface != null)
            {
                Size scaling = new Size(1, 1);
                Size imageSize = this._surface.Size;

                // If we're not stretching or have infinite space, request the full surface size
                if (!(Double.IsInfinity(availableSize.Width) && Double.IsInfinity(availableSize.Height)) &&
                    this._stretchMode != CompositionStretch.None)
                {
                    // Calculate the amount of horizontal and vertical scaling to fit into available space
                    scaling = new Size(availableSize.Width / imageSize.Width, availableSize.Height / imageSize.Height);


                    //
                    // If we've got infinite space in either dimension, scale by the same amount as the constrained
                    // dimension.
                    //

                    if (Double.IsInfinity(availableSize.Width))
                    {
                        scaling.Width = scaling.Height;
                    }
                    else if (Double.IsInfinity(availableSize.Height))
                    {
                        scaling.Height = scaling.Width;
                    }
                    else
                    {
                        //
                        // We're fitting into a space confined by both width and height, do appropriate scaling
                        // based on the stretch mode.
                        //

                        switch (this._stretchMode)
                        {
                            case CompositionStretch.Uniform:
                                scaling.Width = scaling.Height = Math.Min(scaling.Width, scaling.Height);
                                break;
                            case CompositionStretch.UniformToFill:
                                scaling.Width = scaling.Height = Math.Max(scaling.Width, scaling.Height);
                                break;
                            case CompositionStretch.Fill:
                            default:
                                break;
                        }
                    }
                }

                // Apply the scale to get the final desired size
                desiredSize.Width = imageSize.Width * scaling.Width;
                desiredSize.Height = imageSize.Height * scaling.Height;
            }
            else
            {
                // We don't have any content, so default to zero unless a specific size was requested
                if (!Double.IsNaN(Width))
                {
                    desiredSize.Width = Width;
                }
                if (!Double.IsNaN(Height))
                {
                    desiredSize.Height = Height;
                }
            }

            return new Size(Math.Min(availableSize.Width, desiredSize.Width), Math.Min(availableSize.Height, desiredSize.Height));
        }

        private void CompImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this._sprite != null)
            {
                // Calculate the new size
                Vector2 size = new Vector2((float)ActualWidth, (float)ActualHeight);

                // Update the sprite
                this._sprite.Size = size;

                // Update the loading sprite if set
                Visual loadingSprite = this._sprite.Children.FirstOrDefault();
                if (loadingSprite != null)
                {
                    loadingSprite.Size = size;
                }
            }
        }

        private void CompImage_Loading(FrameworkElement sender, object args)
        {
            this._sprite = this._compositor.CreateSpriteVisual();
            this._sprite.Size = new Vector2((float)ActualWidth, (float)ActualHeight);

            // Reset the loading flag
            this._unloaded = false;

            // If the surface is not yet loaded, do so now
            if (!IsContentLoaded)
            {
                LoadSurface();
            }
            else
            {
                // Already had content, just update the brush
                UpdateBrush();
            }

            ElementCompositionPreview.SetElementChildVisual(this, this._sprite);
        }

        private void CompImage_Unloaded(object sender, RoutedEventArgs e)
        {
            this._unloaded = true;

            ReleaseSurface();

            if (this._sprite != null)
            {
                ElementCompositionPreview.SetElementChildVisual(this, null);

                this._sprite.Dispose();
                this._sprite = null;
            }
        }

        private void UpdateBrush()
        {
            this._surfaceBrush.Surface = this._surface;
            this._surfaceBrush.Stretch = this._stretchMode;

            if (this._sprite != null)
            {
                // If the active brush is not set, use the surface brush
                if (this._brush != null)
                {
                    if (this._brush is CompositionEffectBrush)
                    {
                        //
                        // If there is an EffectBrush set, it must supply ImageSource reference parameter for setitng
                        // the Image content.
                        //

                        ((CompositionEffectBrush)this._brush).SetSourceParameter("ImageSource", this._surfaceBrush);
                    }

                    // Update the sprite to use the brush
                    this._sprite.Brush = this._brush;
                }
                else
                {
                    this._sprite.Brush = this._surfaceBrush;
                }
            }
        }

        public Stretch Stretch
        {
            get
            {
                Stretch stretch;

                switch (this._stretchMode)
                {
                    case CompositionStretch.Fill:
                        stretch = Stretch.Fill;
                        break;
                    case CompositionStretch.Uniform:
                        stretch = Stretch.Uniform;
                        break;
                    case CompositionStretch.UniformToFill:
                        stretch = Stretch.UniformToFill;
                        break;
                    default:
                        stretch = Stretch.None;
                        break;
                }

                return stretch;
            }

            set
            {
                CompositionStretch stretch;
                switch (value)
                {
                    case Stretch.Fill:
                        stretch = CompositionStretch.Fill;
                        break;
                    case Stretch.Uniform:
                        stretch = CompositionStretch.Uniform;
                        break;
                    case Stretch.UniformToFill:
                        stretch = CompositionStretch.UniformToFill;
                        break;
                    default:
                        stretch = CompositionStretch.None;
                        break;
                }

                if (stretch != this._stretchMode)
                {
                    this._stretchMode = stretch;

                    if (this._surfaceBrush != null)
                    {
                        this._surfaceBrush.Stretch = stretch;
                    }
                }
            }
        }

        public Uri Source
        {
            get { return this._uri; }
            set
            {
                if (this._uri != value)
                {
                    this._uri = value;
                    LoadSurface();
                }
            }
        }

        public bool IsContentLoaded
        {
            get { return this._surface != null; }
        }

        public bool SharedSurface
        {
            get { return this._sharedSurface; }
            set { this._sharedSurface = value; }
        }

        public LoadTimeEffectHandler LoadTimeEffectHandler
        {
            get { return this._loadEffectDelegate; }
            set
            {
                this._loadEffectDelegate = value;
            }
        }

        private async void LoadSurface()
        {
            // If we're clearing out the content, return
            if (this._uri == null)
            {
                ReleaseSurface();
                return;
            }

            try
            {
                // Start a timer to enable the placeholder image if requested
                if (this._surface == null && this._placeholderDelay >= TimeSpan.Zero)
                {
                    this._timer = new DispatcherTimer();
                    this._timer.Interval = this._placeholderDelay;
                    this._timer.Tick += Timer_Tick;
                    this._timer.Start();
                }

                // Load the image asynchronously
                CompositionDrawingSurface surface = await SurfaceLoader.LoadFromUri(this._uri, Size.Empty, this._loadEffectDelegate);

                if (this._surface != null)
                {
                    ReleaseSurface();
                }

                this._surface = surface;

                // The surface has changed, so we need to re-measure with the new surface dimensions
                InvalidateMeasure();

                // Async operations may take a while.  If we've unloaded, return now.
                if (this._unloaded)
                {
                    ReleaseSurface();
                    return;
                }

                // Update the brush
                UpdateBrush();

                // Success, fire the Opened event
                if (ImageOpened != null)
                {
                    ImageOpened(this, null);
                }

                //
                // If we created the loading placeholder, now that the image has loaded 
                // cross-fade it out.
                //

                if (this._sprite != null && this._sprite.Children.Count > 0)
                {
                    Debug.Assert(this._timer == null);
                    StartCrossFade();
                }
                else if (this._timer != null)
                {
                    // We didn't end up loading the placeholder, so just stop the timer
                    this._timer.Stop();
                    this._timer = null;
                }
            }
            catch (FileNotFoundException)
            {
                if (ImageFailed != null)
                {
                    ImageFailed(this, null);
                }
            }
        }

        private void Timer_Tick(object sender, object e)
        {
            if (this._timer != null)
            {
                Debug.Assert(this._sprite.Children.Count == 0, "Should not be any children");

                // Create a second sprite to show while the image is still loading
                SpriteVisual loadingSprite = this._compositor.CreateSpriteVisual();
                loadingSprite = this._compositor.CreateSpriteVisual();
                loadingSprite.Size = new Vector2((float)ActualWidth, (float)ActualHeight);
                loadingSprite.Brush = this._placeholderBrush != null ? this._placeholderBrush : _defaultPlaceholderBrush;
                this._sprite.Children.InsertAtTop(loadingSprite);

                // Stop and null out the time, no more need for it.
                this._timer.Stop();
                this._timer = null;
            }
        }

        private void StartCrossFade()
        {
            Debug.Assert(this._sprite.Children.Count > 0, "Unexpected number of children");

            // Start a batch so we can cleanup the loading sprite
            CompositionScopedBatch batch = this._compositor.CreateScopedBatch(CompositionBatchTypes.Animation);
            batch.Completed += EndCrossFade;

            // Animate the opacity of the loading sprite to fade it out and the texture scale just for effect
            Visual loadingVisual = this._sprite.Children.LastOrDefault();
            loadingVisual.StartAnimation("Opacity", _fadeOutAnimation);

#if SDKVERSION_14393
            Vector2 visualSize = _sprite.Size;
            _surfaceBrush.CenterPoint = new Vector2(visualSize.X *.5f, visualSize.Y * .5f);

            _surfaceBrush.StartAnimation("Scale", _scaleAnimation);
#endif
            // End the batch after those animations complete
            batch.End();
        }

        private void EndCrossFade(object sender, CompositionBatchCompletedEventArgs args)
        {
            // If the sprite is still valid, remove the loading sprite from the children collection
            if (this._sprite != null && this._sprite.Children.Count > 0)
            {
                this._sprite.Children.RemoveAll();
            }
        }

        public CompositionBrush Brush
        {
            get { return this._brush; }
            set
            {
                this._brush = value;
                UpdateBrush();
            }
        }

        public CompositionBrush PlaceholderBrush
        {
            get { return this._placeholderBrush; }
            set
            {
                this._placeholderBrush = value;

                if (this._sprite != null)
                {
                    // Update the loading sprite if set
                    SpriteVisual loadingSprite = (SpriteVisual)this._sprite.Children.FirstOrDefault();
                    if (loadingSprite != null)
                    {
                        loadingSprite.Brush = this._placeholderBrush;
                    }
                }
            }
        }

        public CompositionSurfaceBrush SurfaceBrush
        {
            get { return this._surfaceBrush; }
        }

        public SpriteVisual SpriteVisual
        {
            get { return this._sprite; }
        }

        public TimeSpan PlaceholderDelay
        {
            get { return this._placeholderDelay; }
            set { this._placeholderDelay = value; }
        }
    }
}
