﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using RearrangeNavigationWithComposition.CompositionAPI;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace RearrangeNavigationWithComposition
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DetailsPage : Page
    {
        private Frame hostFrame;
        private ConnectedTransition currentTransition;
        
        public DetailsPage()
        {
            this.InitializeComponent();

            Loaded += OnLoaded;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            this.currentTransition = (ConnectedTransition)e.Parameter;
            this.hostFrame = (Frame) this.currentTransition.Host;

            // Enable the back button
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
                AppViewBackButtonVisibility.Visible;

            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (this.currentTransition != null)
            {
                // Update the Thumbnail image to point to the proper album art
                this.DetailsImage.Source = new Uri("ms-appx:///Assets/TestImage.jpg");

                // Kick off the transition now that the page has loaded
                this.currentTransition.Start(this.MainGrid, this.DetailsImage, this.MainScrollViewer, this.DummyText);
            }
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            if (!e.Handled)
            {
                // We are about to transition to a new page.  Cancel any outstanding transitions.
                if (this.currentTransition != null)
                {
                    if (!this.currentTransition.Completed)
                    {
                        this.currentTransition.Cancel();
                    }

                    this.currentTransition = null;
                }

                // Setup the new transition and trigger the navigation
                ConnectedTransition transition = new ConnectedTransition();
                transition.Initialize(this.hostFrame, this.DetailsImage, null);

                this.hostFrame.Navigate(typeof(MasterPage), transition);

                // We've got it handled
                e.Handled = true;
            }
        }
    }
}
